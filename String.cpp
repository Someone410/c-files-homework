#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter your name: ";
    std::string PlayerName;
    std::getline(std::cin, PlayerName);

    std::cout << PlayerName  << "\n";
    std::cout << PlayerName.length() << "\n";
    std::cout << PlayerName.front() << "\n" << PlayerName.back() << "\n";
    return 0;
}